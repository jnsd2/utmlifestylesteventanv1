package com.example.utmlifestyle;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class ForgotPasswordDisplayMsg extends AppCompatActivity {

    private Button go_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password_display_msg);

        go_back = findViewById(R.id.goBack);
        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Back();
            }
        });
    }
    public void Back() {
        Intent intent = new Intent(this, MainPage.class);
        startActivity(intent);

    }
}