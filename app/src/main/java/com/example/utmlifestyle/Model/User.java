package com.example.utmlifestyle.Model;

public class User {
    String id;
    String username;

    User(){

    }

    User(String id, String username){
        this.id = id;
        this.username = username;
    }

    public String getId(){
        return this.id;
    }

    public String getUsername(){
        return this.username;
    }
}
