package com.example.utmlifestyle;

import androidx.appcompat.app.AppCompatActivity;

import androidx.annotation.NonNull;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.regex.Pattern;
import android.app.ProgressDialog;
import java.util.HashMap;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Register extends AppCompatActivity {

    private EditText full_name;
    private EditText ic;
    private EditText date_of_birth;
    private EditText Email;
    private EditText phone_number;
    private EditText homeAddress;
    private EditText Password;
    private EditText confirm_password;
    private Button submitButton;
    private Button back;

    private DatabaseReference mRootRef;
    private FirebaseAuth mAuth;

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);



        full_name = findViewById(R.id.fullname);
        ic = findViewById(R.id.ic);
        Email = findViewById(R.id.email);
        date_of_birth = findViewById(R.id.date_of_birth);
        phone_number = findViewById(R.id.phone);
        homeAddress = findViewById(R.id.home_address);
        Password = findViewById(R.id.password);
        confirm_password = findViewById(R.id.confirm_password);
        submitButton = findViewById(R.id.submitBtn);

        mRootRef = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        pd = new ProgressDialog(this);



        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String txtfullname = full_name.getText().toString();
                String txtic = ic.getText().toString();
                String txtdob = date_of_birth.getText().toString();
                String txtEmail = Email.getText().toString();
                String txtphone = phone_number.getText().toString();
                String txtaddress = homeAddress.getText().toString();
                String txtPassword = Password.getText().toString();
                String txtconfirm_password = confirm_password.getText().toString();

                if (TextUtils.isEmpty(txtfullname) || TextUtils.isEmpty(txtic)
                        || TextUtils.isEmpty(txtdob) || TextUtils.isEmpty(txtEmail)
                        || TextUtils.isEmpty(txtphone) || TextUtils.isEmpty(txtaddress)
                        || TextUtils.isEmpty(txtPassword) || TextUtils.isEmpty(txtconfirm_password)) {
                    Toast.makeText(Register.this, "Empty credentials!", Toast.LENGTH_SHORT).show();
                } else if (txtPassword.length() < 6) {
                    Toast.makeText(Register.this, "Password too short!", Toast.LENGTH_SHORT).show();
                } else if (!txtconfirm_password.equals(txtPassword)){
                    Toast.makeText(Register.this, "Password not match!", Toast.LENGTH_SHORT).show();
                }else {
                    registeruser(txtfullname, txtic, txtdob, txtEmail, txtphone, txtaddress , txtPassword, txtconfirm_password);
                }
            }
        });


        back = findViewById(R.id.back_icon);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Back();
            }
        });
    }

    private void registeruser( final String txtfullname, final String txtic, final String txtdob, final String txtEmail, final String txtphone_number,  final String txtaddress , final String txtPassword, final String txtconfirm_password) {


        pd.setMessage("Please Wait!");
        pd.show();

        mAuth.createUserWithEmailAndPassword(txtEmail , txtPassword).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {


                HashMap<String, Object> map = new HashMap<>();
                map.put("id", mAuth.getCurrentUser().getUid());
                map.put("Fullname", txtfullname);
                map.put("ICnumber", txtic);
                map.put("dob", txtdob);
                map.put("email", txtEmail);
                map.put("phone", txtphone_number);
                map.put("address", txtaddress);
                map.put("password", txtPassword);
                mRootRef.child("Users").child(mAuth.getCurrentUser().getUid()).setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            pd.dismiss();
                            Toast.makeText(Register.this, " REGISTRATION SUCCESSFUL " , Toast.LENGTH_SHORT).show();
                            Toast.makeText(Register.this, "Update the profile " +
                                    "for better expereince", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(Register.this, RegisterActivity2.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();

                        }
                    }
                });
            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                pd.dismiss();
                Toast.makeText(Register.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }

           }
        );
    }
    public void Back() {
        Intent intent = new Intent(this, MainPage.class);
        startActivity(intent);

    }

}