package com.example.utmlifestyle;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainPage extends AppCompatActivity {
    Button register;
    Button login;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);

        login = findViewById(R.id.loginBtn);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LOGIN();
            }
        });
        register = findViewById(R.id.regiserBtn);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                REGISTER();
            }
        });
    }
        public void LOGIN() {
            Intent intent = new Intent(this, Login_Avtivity.class);
            startActivity(intent);
        }

        public void REGISTER() {
            Intent intent = new Intent(this, Register.class);
            startActivity(intent);
        }

    }
